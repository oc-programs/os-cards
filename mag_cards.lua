local args = {...}
local event = require("event")
local cmp = require("component")
local read_card_event = "magData"

local function ask_user(question)
  if question then
    io.write(question..": ")
    io.flush()
    local result = io.read()
    io.write("\n")
    return result
  end
end

local function read_data()
  local card_reader = cmp.os_magreader
  print("Waiting for magnetic card")
  local res = table.pack(event.pull(read_card_event))
  print(table.unpack(res))
  return res
end

local function write_data()
  local card_writer = cmp.os_cardwriter
  local card_data = ask_user("Data string to write")
  local disp_data = ask_user("Display string to write")
  local finalize = ask_user("Type \"true\" if you want to finalize card")
  finalize = (finalize == "true")
  if card_data then
    local result = card_writer.write(card_data,disp_data, finalize)
    if result then print("Ok") end
  end
end

local actions = {read = read_data, write = write_data}

local action = actions[args[1]]
if action then
  action()
else
  print("Usage: mag_card read|write")
end
